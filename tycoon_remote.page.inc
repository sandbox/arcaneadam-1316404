<?php
/**
 * Tycoon Remote Pages and such
 */
function tycoon_remote_receipt($txn) {
  return theme('tycoon_remote_receipt', $txn);
}

/**
 * Preprocess function for tycoon_remote_receipt.
 */
function template_preprocess_tycoon_remote_receipt(&$vars) {
  $txn = &$vars['txn'];
  $vars['country_name'] = $txn->country;
  if (module_exists('location')) {
    module_load_include('.inc', 'location', 'location');
    $vars['country_name'] = location_country_name(strtolower($txn->country));
  }
  
  foreach ($txn->activities as $activity) {
    if ($activity->action == 'process' && (!empty($activity->data->mask) && !empty($activity->data->type))) {
      $vars['cc_mask'] = str_pad($activity->data->mask, 15, '*', STR_PAD_LEFT);
      $vars['cc_type'] = ucfirst($activity->data->type);
      break;
    }
  }
  
  usort($txn->line_items, 'tycoon_txn_li_sort');
  //$vars['line_items_table'] = tycoon_remote_li_table($txn->line_items, $txn->total);
}

/**
 * Sorts Line Items in a transaction by total price in DESC order.
 */
function tycoon_txn_li_sort($a, $b) {
  $a_total = $a->price * $a->qty;
  $b_total = $b->price * $b->qty;
  if ($a_total == $b_total) {
    return 0;
  }
  return ($a_total < $b_total) ? 1 : -1;
}

/**
 * Outputs the line_items table
 */
function tycoon_remote_li_table($line_items, $total) {
  $format = '%.2n';
  foreach ($line_items as $item) {
    $row = array();
    $row[] = t($item->title);
    $row[] = money_format($format, $item->price * $item->qty);
    $rows[] = $row;
  }
  $rows[] = array(
    t('Total'),
    money_format($format, $total),
  );
  
  return theme('table', array(), $rows);
}

/**
 * Menu callback; Retrieve a JSON object containing autocomplete suggestions for existing users.
 */
function tycoon_remote_autocomplete($string = '') {
  $matches = array();
  if ($string) {
    $sql = "SELECT n.title FROM {tycoon_remote_transaction_node} t 
    JOIN {remote_node} r ON r.rnid = t.rnid 
    JOIN {node} n ON n.nid = r.nid 
    JOIN {tycoon_remote_transaction} tt ON tt.txid = t.txid
    WHERE n.title LIKE '%%%s%%'
    ";
    $result = db_query_range($sql, $string, 0, 10);
    while ($node = db_fetch_object($result)) {
      $matches[$node->title] = check_plain($node->title);
    }
  }
  drupal_json($matches);
}
