<?php
/**
 * Themes the receipt page for a transaction
 *
 * @param $txn
 *   The transaction object.
 */
?>
<div class="receipt">
  <div class="back-to-giving right">
    <?php  print l('«Back to Donation History', 'giving/history');?>
  </div>
  <div class="row receipt-np">
    <span class="label">Nonprofit Organization:</span> <?php  print $txn->np->title;?>
  </div>
  <div class="row receipt-date">
    <span class="label">Date:</span> <?php  print format_date($txn->created, 'custom', 'm/d/Y');?>
  </div>
  <div class="row receipt-id">
    <span class="label">ID:</span> <?php  print $txn->txid;?>
  </div>
  <div class="section">
    <div class="label">
      Details:
    </div>
    <div class="row receipt-name">
      <?php  print $txn->first_name;?> <?php  print $txn->last_name;?>
    </div>
    <div class="row receipt-street1">
      <?php  print $txn->street1;?>
    </div>
    <?php if (!empty($txn->street2)):?>
    <div class="row receipt-street2">
      <?php  print $txn->street2;?>
    </div>
    <?php  endif;?>
    <div class="row receipt-ctystzip">
      <span class="receipt-city"><?php  print $txn->city;?></span>, <span class="receipt-st"><?php print $txn->province;?></span><span class="receipt-zip"><?php print $txn->postal_code;?></span>
    </div>
    <div class="row receipt-country">
      <?php print $country_name;?>
    </div>
  </div>
  <?php if ($cc_mask && $cc_type): ?>
  <div class="section">
    <div class="label">
      Payment Method:
    </div>
    <div class="row receipt-ccinfo"><span class="receipt-cctype"><?php print $cc_type?>:</span> <span class="receipt-ccmask"><?php print $cc_mask?></span></div>
  </div>
  <?php endif; ?>
  <div class="section">
    <div class="label">
      Summary:
    </div>
    <?php setlocale(LC_MONETARY, 'en_US'); ?>
    <?php foreach ($txn->line_items as $line_item):?>
    <?php /*I know it's not the best idea to have logic in a TPL but this seems like the best way to accomplish the following. Rather then creating a seperate TPL just for a line item.*/?>
      <div class="row receipt-line_item">
        <span class="label"><?php print $line_item->title;?>:</span> <span class="total money"><?php print money_format('%.2n',($line_item->price * $line_item->qty));?></span>
      </div>
    <?php endforeach;?>
      <div class="row receipt-total">
        <span class="label">Total Transaction:</span> <span class="total money"><?php print money_format('%.2n', $txn->total);?></span>
      </div>
  </div>
</div>
