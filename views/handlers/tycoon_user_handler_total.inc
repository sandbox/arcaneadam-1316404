<?php
class tycoon_user_handler_total extends views_handler_field_money {
  function option_definition() {
    $options = parent::option_definition();
    $options['group_by'] = array('default' => TRUE);
     
    return $options;
  }
  
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    
    $form['group_by'] = array(
      '#title' => t('Group Values By User ID'),
      '#type' => 'checkbox',
      '#default_value' => $this->options['group_by'],
      '#description' => t('This sets up the Group By field in the query'),
    );
  }
  
  function query() {
    $this->ensure_my_table();
    // Add the field.
    $join = new views_join();
    $join->definition = array(
      'table' => 'tycoon_remote_transaction',
      'field' => 'uid',
      'left_table' => 'user',
      'left_field' => 'uid',
      'type' => 'LEFT',
    );
    $join->construct();
    $txn_alias = $this->query->ensure_table('tycoon_remote_transaction', $this->relationship, $join);
    $field = "{$txn_alias}.total";
    $sql = "SUM({$field})";
    $this->field_alias = $this->query->add_field(NULL, $sql, "{$this->table_alias}_tycoon_total", array('aggregate' => TRUE));
    if (!empty($this->options['group_by'])) {  
      $this->query->has_aggregate = TRUE;
      $groupby = $this->query->add_field($this->table_alias, "uid");
      $this->query->add_groupby($groupby);
    }
    $this->add_additional_fields();
  }
}