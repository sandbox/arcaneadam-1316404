<?php
class tycoon_node_handler_count extends views_handler_field_numeric {

  function query() {
    $this->ensure_my_table();
    // Add the field.
    $sql = "COUNT({$this->table_alias}.txid)";
    $this->field_alias = $this->query->add_field(NULL, "({$sql})", 'total_txns');
    $this->add_additional_fields();
  }
}
