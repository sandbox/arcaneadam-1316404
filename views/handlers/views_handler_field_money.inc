<?php
/**
 * @defgroup views_field_handlers Views' field handlers
 * @{
 * Handlers to tell Views how to build and display fields.
 *
 */
class views_handler_field_money extends views_handler_field {
  
  function option_definition() {
    $options = parent::option_definition();
     
    $options['money_locale'] = array('default' => 'en_US');
    $options['money_format'] = array('default' => '%.2n');
    $options['money_custom'] = array('default' => '$');
    $options['money_custom_format'] = array('default' => TRUE);
     
    return $options;
  }

  /**
   * Default options form that provides the format and country
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['money_format'] = array(
      '#title' => t('Currency Format'),
      '#type' => 'select',
      '#options' => $this->money_formats(),
      '#default_value' => $this->options['money_format'],
      '#description' => t('The way the money should be formatted. All examples are using a value of 1234.56'),
    );
    $form['money_locale'] = array(
      '#title' => t('Currency Type'),
      '#type' => 'select',
      '#options' => $this->currency_types(),
      '#default_value' => $this->options['money_locale'],
      '#description' => t('The currency to use. i.e. (Dollars => USD, Euros => EUR, etc...)'),
      '#process' => array('views_process_dependency'),
      '#dependency' => array(
          'edit-options-money-format' => array('%i', '%.2n', '%.0n'),
        ),
    );
    $form['money_custom'] = array(
      '#type' => 'textfield',
      '#title' => t('Cusotm Prefix'),
      '#size' => 5,
      '#default_value' => $this->options['money_custom'],
      '#description' => t('A custom string to prefix the number with.'),
      '#process' => array('views_process_dependency'),
      '#dependency' => array(
          'edit-options-money-format' => array('custom'),
        ),
    );
    $form['money_custom_format'] = array(
      '#type' => 'checkbox',
      '#title' => t('Format Number'),
      '#default_value' => $this->options['money_custom_format'],
      '#description' => t('Format the number so that 1234 becomes 1,234.00'),
      '#process' => array('views_process_dependency'),
      '#dependency' => array(
          'edit-options-money-format' => array('custom'),
        ),
    );
  }
  
  /**
   * Returns the money format options
   */
  function money_formats() {
    return array(
      '%i' => 'USD 1,234.56',
      '%.2n' => '$1,234.56',
      '%.0n' => '$1,234',
      'custom' => 'Custom prefix',
    );
  }
  
  /**
   * Creates the Locale options
   */
  function currency_types() {
    static $currencies;
    if (isset($currencies)) {
      return $currencies;
    }
    $orig_locale = setlocale(LC_MONETARY, '');
    exec('locale -a', $locales);
    $saved = array();
    foreach ($locales as $locale) {
      setlocale(LC_MONETARY, $locale);
      $currency = money_format('%i', 0);
      $currency = trim(preg_replace('/[0-9.,]/','',$currency));
      if (!$currency || isset($saved[$currency])) {continue;}
      $saved[$currency] = TRUE;
      $currencies[$locale] = $currency;
    }
    setlocale(LC_MONETARY, $orig_locale);
    asort($currencies);
    return $currencies;
  }
  
  /**
   * Render the field.
   *
   * @param $values
   *   The values retrieved from the database.
   */
  function render($values) {
    if ($this->options['money_format'] != 'custom') {
      $locale = setlocale(LC_MONETARY, '');
      $value = $this->get_value($values);
      setlocale(LC_MONETARY, $this->options['money_locale']);
      $value = money_format($this->options['money_format'], $value);
      setlocale(LC_MONETARY, $locale);
    }
    else {
      $value = $this->get_value($values);
      if ($this->options['money_custom_format'] == TRUE) {
        $value = number_format($value, 2);
      }
      $value = $this->options['money_custom'] . $value;
    }
    return $this->sanitize_value($value);
  }
}