<?php
/**
 * Abstract argument handler for dates.
 *
 * Adds an option to set a default argument based on the current date.
 *
 * @param $arg_format
 *   The format string to use on the current time when
 *   creating a default date argument.
 *
 * Definitions terms:
 * - many to one: If true, the "many to one" helper will be used.
 * - invalid input: A string to give to the user for obviously invalid input.
 *                  This is deprecated in favor of argument validators.
 * @see views_many_to_one_helper
 *
 * @ingroup views_argument_handlers
 */
class tycoon_handler_argument_fresh extends views_handler_argument {
  
  /**
   * Build the query based upon the formula
   */
  function query($group_by = FALSE) {
    $this->ensure_my_table();

    $null_check = empty($this->options['not']) ? '' : "OR $this->table_alias.$this->real_field IS NULL";

    $time = strtotime($this->argument);
    $this->query->add_where(0, "$this->table_alias.$this->real_field >= %d $null_check", $time);
  }
  
}
