<?php
class tycoon_line_item_handler_total extends views_handler_field_money {

  function query() {
    $this->ensure_my_table();
    // Add the field.
    $sql = "{$this->table_alias}.qty * {$this->table_alias}.price";
    $this->field_alias = $this->query->add_field(NULL, "({$sql})", 'total');
    $this->add_additional_fields();
  }
  
}
