<?php
// $Id:
/**
 * @file
 * Provide views data for tycoon_remote.module
 */


/**
 * Implementation of hook_views_data()
 */
function tycoon_remote_views_data() {
  /* 
   * TABLE tycoon_remote_transaction
   */	
	
  $data['tycoon_remote_transaction']['table']['group']  = t('Tycoon Remote Transaction');
  $data['tycoon_remote_transaction']['table']['base'] = array(
    'field' => 'txid',
    'title' => t('Tycoon Remote Transactions'),
    'help' => t("Tycoon Remote transactions collected throught Tycoon API."),
  );
  
  $data['tycoon_remote_transaction']['table']['join'] = array(
    'users' => array(
      'left_field' => 'uid',
      'field' => 'uid',
    ),   
  );
  
  
  $data['tycoon_remote_transaction']['txid'] = array(
    'title' => t('Transaction ID'),
    'help' => t('The ID of the transaction.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
      'allow empty' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'relationship' => array(
      'base' => 'tycoon_remote_transaction_node',
      'handler' => 'views_handler_relationship',
      'label' => t('Transaction ID'),
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
  );
  
  $data['tycoon_remote_transaction']['txn_count'] = array(
    'title' => t('Transaction Count'),
    'help' => t('The ID of the transaction.'),
    'field' => array(
      'handler' => 'tycoon_user_handler_txn_count',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
      'allow empty' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );  
  
  $data['tycoon_remote_transaction']['state'] = array(
    'title' => t('State'),
    'help' => t('The current state of the transaction.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
      'allow empty' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    
  );
  
  $data['tycoon_remote_transaction']['total'] = array(
    'title' => t('Total'),
    'help' => t('The total of the transaaction'),
    'field' => array(
      'handler' => 'views_handler_field_money',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
      'allow empty' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  ); 

  $data['tycoon_remote_transaction']['total_paid'] = array(
    'title' => t('Total Paid'),
    'help' => t('The total paid for the transaaction'),
    'field' => array(
      'handler' => 'views_handler_field_money',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
      'allow empty' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );   
  

  $data['tycoon_remote_transaction']['currency'] = array(
    'title' => t('Currency'),
    'help' => t('The currency of the transaaction'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
      'allow empty' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );     
  
  $data['tycoon_remote_transaction']['uid'] = array(
    'title' => t('User'),
    'help' => t('Relate a transaction to the user who created it.'),
    'relationship' => array(
      'handler' => 'views_handler_relationship',
      'base' => 'users',
      'base field' => 'uid',
      'label' => t('user'),
    ),
  );
  
  $data['tycoon_remote_transaction']['mail'] = array(
    'title' => t('Email'),
    'help' => t('The user\'s email for the transaction.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
      'allow empty' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  
  $data['tycoon_remote_transaction']['first_name'] = array(
    'title' => t('First Name'),
    'help' => t('The user\'s first name of the transaction.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
      'allow empty' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
    
  
  $data['tycoon_remote_transaction']['last_name'] = array(
    'title' => t('Last Name'),
    'help' => t('The user\'s last name of the transaction.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
      'allow empty' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  
  $data['tycoon_remote_transaction']['phone'] = array(
    'title' => t('Phone'),
    'help' => t('The user\'s phone number for the transaction.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
      'allow empty' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
      
  $data['tycoon_remote_transaction']['company'] = array(
    'title' => t('Company'),
    'help' => t('The user\'s company for the transaction.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
      'allow empty' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  
  
  $data['tycoon_remote_transaction']['street1'] = array(
    'title' => t('Street Address 1'),
    'help' => t('The user\'s street address for the transaction.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
      'allow empty' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  
  $data['tycoon_remote_transaction']['street2'] = array(
    'title' => t('Street Address 2'),
    'help' => t('The user\'s second line street address for the transaction.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
      'allow empty' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

    
  $data['tycoon_remote_transaction']['city'] = array(
    'title' => t('City'),
    'help' => t('The user\'s city for the transaction.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
      'allow empty' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
    
  
  $data['tycoon_remote_transaction']['province'] = array(
    'title' => t('First Name'),
    'help' => t('The user\'s province for the transaction.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
      'allow empty' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  
  
  $data['tycoon_remote_transaction']['postal_code'] = array(
    'title' => t('Postal Code'),
    'help' => t('The user\'s postal code for the transaction.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
      'allow empty' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
      
  $data['tycoon_remote_transaction']['country'] = array(
    'title' => t('Country'),
    'help' => t('The user\'s country for the transaction.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
      'allow empty' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['tycoon_remote_transaction']['created'] = array(
    'title' => t('Created Date'),
    'help' => t('The creation date of the transaaction'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
      'allow empty' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
  ); 

  $data['tycoon_remote_transaction']['last_donation'] = array(
    'title' => t('Last Donation'),
    'help' => t('The last donation date'),
    'field' => array(
      'handler' => 'tycoon_user_handler_last_donated',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
      'allow empty' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
  );   
  
  $data['tycoon_remote_transaction']['updated'] = array(
    'title' => t('Updated Date'),
    'help' => t('The updated date of the transaaction'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
      'allow empty' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );     

  $data['tycoon_remote_transaction']['data'] = array(
    'title' => t('Data'),
    'help' => t('The data of the transaaction'),
    'field' => array(
      'handler' => 'views_handler_field_serialized',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
      'allow empty' => TRUE,
    ),
  );
  
  $data['tycoon_remote_transaction']['created_arg'] = array(
    'title' => t('Transaction Freshness'),
    'help' => t('How new the transaction is'),
    'argument' => array(
      'real field' => 'created',
      'handler' => 'tycoon_handler_argument_fresh',
    ),
  );

  /* 
   * TABLE tycoon_remote_transaction_node
   */
  
  
  $data['tycoon_remote_transaction_node']['table']['group']  = t('Tycoon Remote Transaction Node');

  $data['tycoon_remote_transaction_node']['table']['join'] = array(
    'tycoon_remote_transaction' => array(
      'left_field' => 'txid',
      'field' => 'txid',
    ),
    'remote_node' => array(
      'left_field' => 'rnid',
      'field' => 'rnid',
      'type' => 'INNER',
    ),    
  );

  $data['tycoon_remote_transaction_node']['txid'] = array(
    'title' => t('Transaction ID'),
    'help' => t('The transaction ID for the line item.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
      'allow empty' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'relationship' => array(
      'base' => 'tycoon_remote_transaction',
      'handler' => 'views_handler_relationship',
      'label' => t('Transaction ID'),
      'base field' => 'txid',
      'field' => 'txid',
    ),
  );
  
  $data['tycoon_remote_transaction_node']['rnid'] = array(
    'title' => t('Node ID'),
    'help' => t('The Remote Node ID for the transaction.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
      'allow empty' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'relationship' => array(
      'base' => 'remote_node',
      'handler' => 'views_handler_relationship',
      'label' => t('Remote Node ID'),
      'base field' => 'rnid',
      'field' => 'rnid',  
    ),
  );
  
  $data['tycoon_remote_transaction_node']['total'] = array(
    'title' => t('Total Value'),
    'help' => t('The total value of all transactions for a node'),
    'field' => array(
      'handler' => 'tycoon_node_handler_total',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
      'allow empty' => TRUE,
    ),       
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  
  $data['tycoon_remote_transaction_node']['total_txn'] = array(
    'title' => t('Total transactions'),
    'help' => t('The total number of transactions for a node'),
    'field' => array(
      'handler' => 'tycoon_node_handler_count',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
      'allow empty' => TRUE,
    ),       
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  
  $data['tycoon_remote_transaction_node']['nid_auto'] = array(
    'title' => t('Donation Node title'),
    'help' => t('An autocomplete field for node titles'),
    'real field' => 'rnid',
    'filter' => array(
      'handler' => 'tycoon_remote_filter_node_auto',
      'allow empty' => TRUE,
    ),
  );
  
  /* 
   * TABLE tycoon_remote_line_item
   */
    
  $data['tycoon_remote_line_item']['table']['group']  = t('Tycoon Remote Line Item');

  $data['tycoon_remote_line_item']['table']['join'] = array(
    'tycoon_remote_transaction' => array(
      'left_field' => 'txid',
      'field' => 'txid',
    ),
  );
  
  $data['tycoon_remote_line_item']['txid'] = array(
    'title' => t('Transaction ID'),
    'help' => t('The transaction ID for the line item.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
      'allow empty' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'relationship' => array(
      'base' => 'tycoon_remote_transaction',
      'handler' => 'views_handler_relationship',
      'label' => t('Transaction ID'),
      'base field' => 'txid',
      'field' => 'txid',
    ),
  );
  
  $data['tycoon_remote_line_item']['identifier'] = array(
    'title' => t('Identifier'),
    'help' => t('The identifier for the line item.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
      'allow empty' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),    
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  
  $data['tycoon_remote_line_item']['title'] = array(
    'title' => t('Title'),
    'help' => t('The title of the line item.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
      'allow empty' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),       
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  
  $data['tycoon_remote_line_item']['description'] = array(
    'title' => t('Description'),
    'help' => t('The description of the line item.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
      'allow empty' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),       
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['tycoon_remote_line_item']['qty'] = array(
    'title' => t('Quantity'),
    'help' => t('The quantity of the line item.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
      'allow empty' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),       
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  
  $data['tycoon_remote_line_item']['price'] = array(
    'title' => t('Price'),
    'help' => t('The price of unit.'),
    'field' => array(
      'handler' => 'views_handler_field_money',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_float',
      'parent' => 'views_handler_filter_numeric',
      'allow empty' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),       
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  
  $data['tycoon_remote_line_item']['total'] = array(
    'title' => t('Total Price'),
    'help' => t('The total price of the line item. (price x quantity)'),
    'field' => array(
      'handler' => 'tycoon_line_item_handler_total',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
      'allow empty' => TRUE,
    ),       
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  
  $data['tycoon_remote_line_item']['taxable'] = array(
    'title' => t('Taxable'),
    'help' => t('The taxability of the line item.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
      'allow empty' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),       
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );  

  $data['tycoon_remote_line_item']['weight'] = array(
    'title' => t('Weight'),
    'help' => t('The weight of the line item.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
      'allow empty' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),       
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );  
  
  $data['tycoon_remote_line_item']['data'] = array(
    'title' => t('Data'),
    'help' => t('The data of the line item.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
      'allow empty' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),       
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );  
  
  
  return $data;
}

/**
 * Implements hook_views_handlers().
 */
function tycoon_remote_views_handlers() { 
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'tycoon_remote') . '/views/handlers',
    ),
    'handlers' => array(
      // field handlers
      'views_handler_field_money' => array(
        'parent' => 'views_handler_field',
      ),
      'tycoon_line_item_handler_total' => array(
        'parent' => 'views_handler_field_money',
      ),
      'tycoon_node_handler_total' => array(
        'parent' => 'views_handler_field_money',
      ),
      'tycoon_node_handler_count' => array(
        'parent' => 'views_handler_field_numeric',
      ),
      'tycoon_remote_filter_node_auto' => array(
        'parent' => 'views_handler_filter_string',
      ),
      'tycoon_handler_argument_fresh' => array(
        'parent' => 'views_handler_argument',
      ),
      'tycoon_user_handler_total' => array(
        'parent' => 'views_handler_field_money',
      ),
      'tycoon_user_handler_last_donated' => array(
        'parent' => 'views_handler_field_date',
      ),      
      'tycoon_user_handler_txn_count' => array(
        'parent' => 'views_handler_field_numeric',
      ),      
    ),
  );
}

/**
 * Implements hook_views_data_alter
 */
function tycoon_remote_views_data_alter(&$data) {
  //We need to alter the users table so it's available to the transaction table
  $data['users']['table']['join'] = array(
    'tycoon_remote_transaction' => array(
      'left_field' => 'uid',
      'field' => 'uid',
      'type' => 'INNER', // all transactions have an user.
    ),
  );
  
  $data['users']['tycoon_total'] = array(
    'title' => t('Total transaction value'),
    'help' => t('The total value of transactions for a User'),
    'field' => array(
      'real field' => 'uid',
      'handler' => 'tycoon_user_handler_total',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
      'allow empty' => TRUE,
    ),       
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  
  $data['node']['nid_auto'] = array(
    'title' => t('Donation Node title'),
    'help' => t('An autocomplete field for node titles attached to donations'),
    'real field' => 'title',
    'filter' => array(
      'handler' => 'tycoon_remote_filter_node_auto',
      'allow empty' => TRUE,
    ),
  );
}
